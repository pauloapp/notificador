<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioMensagem extends Model
{
    protected $table = 'usuario_mensagem';
    
    protected $fillable=['mensagem_id', 'usuario'];
    
    public function mensagem(){
        return $this->belongsTo('App\Mensagem');
    }
}
