<?php

namespace App\Http\Controllers;

use App\Mensagem;
use App\UsuarioMensagem;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class MensagemController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $id = Auth::id();

        if (Auth::guest()) {
            return redirect()->guest('login');
        }

        $mensagens = Mensagem::where('user_id', $id)->get();
        return view('mensagem.index', ['mensagensusuario' => $mensagens]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('mensagem.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {

            if (Auth::guest()) {
                return redirect()->guest('login');
            }

            $id = Auth::id();
            $mensagem = $request->all();

            $chaveAnteriorSite = Mensagem::where([['site', '=', $request->site], ['user_id', '=', $id]])->first();

            if ($chaveAnteriorSite != null) {
                $mensagem['chave'] = $chaveAnteriorSite['chave'];
                $mensagem['codigo'] = $chaveAnteriorSite['codigo'];
            } else {
                $chaveAnterior = Mensagem::where('user_id', $id)->get();

                if (count($chaveAnterior) > 0)
                    $mensagem['codigo'] = $chaveAnterior[0]['codigo'];
                else
                    $mensagem['codigo'] = str_pad(rand(1, 999999999999), 12, '0', STR_PAD_LEFT);

                $mensagem['chave'] = str_pad(rand(1, 99999999999999999999), 20, '0', STR_PAD_LEFT);
            }

            if (isset($mensagem['permanente'])){
                $mensagem['permanente'] = $permanente = true;
            } else {
                $mensagem['permanente'] = $permanente = false;
            }

            $mensagem['user_id'] = $id;

            if (!empty($mensagem['key'])) {
                $mensagem = Mensagem::where('id', $mensagem['key'])->first();
                $mensagem->descricao = $request->descricao;
                $mensagem->site = $request->site;
                $mensagem->permanente = $permanente;
                $mensagem->usuarios_permitidos = $request->usuarios_permitidos;
                $mensagem->save();
            } else {
                Mensagem::create($mensagem);
            }
            return redirect('mensagem')->with('message', 'Mensagem criada com sucesso!');
        } catch (Exception $exc) {
            return back()->with('error', 'Não foi possível realizar a operação');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bar  $bar
     * @return \Illuminate\Http\Response
     */
    public function show(Mensagem $mensagem) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bar  $bar
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (Auth::guest()) {
            return redirect()->guest('login');
        }
        $mensagem = Mensagem::where('id', $id)->first();
        $mensagem = Mensagem::where('id', $id)->first();
        if (!$mensagem) {
            abort(404);
        }

        return view('mensagem.edit', ['dadosMensagem' => $mensagem]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bar  $bar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        /*
          $this->validate($request, [
          'nome' => 'required',
          'descricao' => 'required',
          ]);
         */
        //$mensagem = Mensagem::where('id', $id)->first();
        //$mensagem->descricao = $request->descricao;
        //$mensagem->site = $request->site;
        //$mensagem->save();
        //return redirect('mensagem')->with('message', 'Mensagem editado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bar  $bar
     * @return \Illuminate\Http\Response
     */
    public function remove($id) {
        try {
            if (Auth::guest()) {
                return redirect()->guest('login');
            }
            
            UsuarioMensagem::where('mensagem_id', $id)->delete();
            Mensagem::where('id', $id)->delete();
            
             return redirect('mensagem')->with('message', 'Mensagem criada com sucesso!');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function delete($id) {
        if (Auth::guest()) {
            return redirect()->guest('login');
        }

        $mensagem = Mensagem::where('id', $id)->first();
        if (!$mensagem) {
            abort(404);
        }

        return view('mensagem.destroy', ['dadosMensagem' => $mensagem]);
    }

}
