<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Mensagem;
use App\UsuarioMensagem;
use Illuminate\Http\Resources\Json\Resource;

class ScriptController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        
    }

    public function getMensagem(Request $request) {
        try {
            $url = $request['url'];

            if (!empty($request['usuario'])) { 
                $mensagensUsuario = Mensagem::
                    Where([['codigo', '=', $request['codigo']], ['chave', '=', $request['chave']], ['usuarios_permitidos', 'LIKE', '%' . $request['usuario'] . '%']])
                        ->orWhere([['codigo', '=', $request['codigo']], ['chave', '=', $request['chave']]])
                        ->whereNull('usuarios_permitidos')
                        ->get();
                
                $mensagemVisualizadas = UsuarioMensagem::where('usuario', $request['usuario'])->get();
                $mensagensAtivas = array();

                foreach ($mensagensUsuario as $mensagemUsuario) {
                    $inativa = false;

                    foreach ($mensagemVisualizadas as $visualizada) {
                        if ($visualizada['mensagem_id'] == $mensagemUsuario['id'])
                            $inativa = true;
                    }

                    if (!$inativa)
                        array_push($mensagensAtivas, $mensagemUsuario);
                }

                foreach ($mensagensAtivas as $mens)
                    $retorno[] = array($request['codigo'], $request['chave'], $request['usuario'], $mens['descricao'], $mens['id'], $mens['permanente']);
            } else {
                $mensagensUsuario = Mensagem::
                    where([['codigo', '=', $request['codigo']], ['chave', '=', $request['chave']]])
                    ->whereNull('usuarios_permitidos')
                    ->get();
                
                foreach ($mensagensUsuario as $mens)
                    $retorno[] = array($request['codigo'], $request['chave'], $request['usuario'], $mens['descricao'], $mens['id'], true);
            }
            return json_encode($retorno);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function setMensagemLida(Request $request) {
        try {
            $mensagens = Mensagem::where([['codigo', '=', $request['codigo']], ['chave', '=', $request['chave']], ['id', '=', $request['id']]])->first();

            if (!empty($mensagens)) {
                $usuarioMensagem['usuario'] = $request['usuario'];
                $usuarioMensagem['mensagem_id'] = $mensagens['id'];

                UsuarioMensagem::create($usuarioMensagem);
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}
