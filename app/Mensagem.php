<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensagem extends Model
{
    protected $table = 'mensagem';
    
    protected $fillable=['codigo', 'descricao', 'site', 'chave', 'user_id', 'usuarios_permitidos', 'permanente'];
    
    public function user(){
        return $this->belongsTo('App\User');
    }
}
