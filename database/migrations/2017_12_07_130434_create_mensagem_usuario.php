<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensagemUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_mensagem', function (Blueprint $table) {
            $table->increments('id');
            $table->string('usuario');
            $table->integer('mensagem_id')->unsigned()->index();
            $table->timestamps();
            $table->foreign('mensagem_id')->references('id')->on('mensagem');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('usuario_mensagem');
    }
}
