<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['web']], function() {
    Route::resource('mensagem', 'MensagemController');
});

Route::get('mensagem/destroy/{id}' ,array(
    'as'=>'mensagem.destroy',
    'uses'=>'MensagemController@delete'));


Route::post('mensagem/destroy/{id}' ,array(
    'as'=>'mensagem.remove',
    'uses'=>'MensagemController@remove'));

Route::get('/criarpopups', 'ScriptController@getMensagem');
Route::get('/informarleitura', 'ScriptController@setMensagemLida');

//Route::get('/criarpopups', ['middleware' => 'cors' , 'uses'=> 'ScriptController@getMensagem']);
//Route::get('/criarpopups', array('middleware' => 'cors', 'uses' => 'ScriptController@getMensagem'));
//Route::get('/informarleitura', array('middleware' => 'cors', 'uses' => 'ScriptController@setMensagemLida'));
