link = document.createElement('link');
link.href = 'https://notificador.binovacao.com.br/css/notificador.css';
link.rel = 'stylesheet';
document.getElementsByTagName('head')[0].appendChild(link);

if (!window.jQuery) {
    document.writeln('<script src=\"https://notificador.binovacao.com.br/js/jquery-3.2.1.min.js\" type="text/javascript"><\/script>');
}

function getpopupNotificadorAuto(codigo, chave, usuario) {
    var current_url = window.location.hostname;
    var path_service = "https://notificador.binovacao.com.br/criarpopups";
    
    $.ajax({
        type: 'GET',
        url: path_service,
        data: {codigo: codigo, chave: chave, usuario: usuario, url: current_url},
        success: function(data) {
            data = jQuery.parseJSON(data);
            $.each(data, showModalNotificadorAuto);
        }
    });
}

function showModalNotificadorAuto(index, item) {
    var funcao = "removePopUpNotificadorAuto('" + item[0] + "','" + item[1] + "','" + item[2] + "','" + item[4] + "','" + index + "')";
    var permanente = '';
     var btn = decode_utf8("Estou ciente, não exibir mais essa mensagem");
    
    if(item[5] == true)
        permanente = 'display: none;';

    var menu = document.createElement('div');
    menu.id = 'teste';
    menu.innerHTML = '<div id="notificadorModal'+index+'" class="notificador"><div class="notificador-content"><p style="font-family: Arial, Helvetica, sans-serif;">' + item[3] + '</p></br><a href="#" style="text-decoration : none; color : #5F9EA0; font-family: Arial, Helvetica, sans-serif;' + permanente + '" id="desativarMensagem' + index + '" onClick="' + funcao + '"><strong>' + btn + '</strong></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" rel="modalnotificador:close" onClick="desativarModal('+index+')" style="text-decoration : none; color :#5F9EA0; font-family: Arial, Helvetica, sans-serif;"><strong>Fechar</strong></a></div></div>';
    document.body.appendChild(menu);
}

function desativarModal(index){
    document.getElementById('notificadorModal'+index).remove();
}

function removePopUpNotificadorAuto(codigo, chave, usuario, id, index){
    var path_service = "https://notificador.binovacao.com.br/informarleitura";
    var current_url = window.location.hostname;

    $.ajax({
        type: 'GET',
        url: path_service,
        data: {codigo: codigo, chave: chave, usuario: usuario, url: current_url, id: id}
    });
    
    document.getElementById('notificadorModal'+index).remove();
}

function decode_utf8(s) {
    try {
        return decodeURIComponent(escape(s));
    } catch (e) {
        return s;
    }
}