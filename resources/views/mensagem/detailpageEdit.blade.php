@extends('layouts.app')

@section('content')
<h1>Atualização realizada com sucesso</h1>
<h2>{{ $detailpage->nome }}</h2>
<p>
    {{ $detailpage->descricao }}
</p>
<a href="/mensagem">Voltar</a>
@endsection