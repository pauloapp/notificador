@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><strong>EXCLUSÃO DE MENSAGENS</strong></div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('mensagem.remove', ['id'=> $dadosMensagem->id]) }}">
                         <input type="hidden" id="key" name="key" value="{{$dadosMensagem->id}}">
                        {{ csrf_field() }}
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <p>{{ \Session::get('success') }}</p>
                            </div><br />
                        @endif
                        <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
                            <label for="descricao" class="col-md-4 control-label">Descrição: </label>
                            <label for="descricao" class="control-label">{{ str_limit($dadosMensagem->descricao, $limit = 50, $end = '............')  }}</label>
                        </div>
                        <div class="form-group{{ $errors->has('site') ? ' has-error' : '' }}">
                            <label for="site" class="col-md-4 control-label">Site: </label>
                            <label for="site" class="control-label">{{ $dadosMensagem->site }}</label>
                        </div>
                         <div class="form-group{{ $errors->has('usuarios_permitidos') ? ' has-error' : '' }}">
                            <label for="usuarios_permitidos" class="col-md-4 control-label">Usuário(s) Permitido(s): </label>
                            <label for="descricao" class="control-label">{{ $dadosMensagem->usuarios_permitidos }}</label>
                        </div>
                        <div class="form-group{{ $errors->has('permanente') ? ' has-error' : '' }}">
                            <label for="permanente" class="col-md-4 control-label">Permanente: </label>
                            <label for="descricao" class="control-label">{{ ($dadosMensagem->permanente ? 'SIM' : 'NÃO') }}</label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-danger">
                                    Excluir
                                </button>
                                <a href="{{URL::previous()}}" class="btn btn-primary">Voltar</a>
                            </div>
                        </div>
                    </form>

                    <div class="row">
                    <ul>
                    
                    </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
