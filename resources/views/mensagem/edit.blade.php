@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><strong>EDIÇÃO DAS MENSAGENS</strong></div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('mensagem.store') }}">
                         <input type="hidden" id="key" name="key" value="{{$dadosMensagem->id}}">
                        {{ csrf_field() }}
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <p>{{ \Session::get('success') }}</p>
                            </div><br />
                        @endif
                        <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
                            <label for="descricao" class="col-md-4 control-label">Mensagem</label>
                           
                            <div class="col-md-6">
                                <textarea id="descricao"  class="form-control" name="descricao"  required autofocus >{{ $dadosMensagem->descricao }}</textarea>
                                
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('site') ? ' has-error' : '' }}">
                            <label for="site" class="col-md-4 control-label">Site</label>
                            <div class="col-md-6">
                                <input id="site" type="text" class="form-control" name="site" value="{{ $dadosMensagem->site }}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('usuarios_permitidos') ? ' has-error' : '' }}">
                            <label for="usuarios_permitidos" class="col-md-4 control-label">Usuário(s) Permitido(s)</label>
                            <div class="col-md-6">
                                <textarea id="usuarios_permitidos"  class="form-control" name="usuarios_permitidos">{{ $dadosMensagem->usuarios_permitidos }}</textarea>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('permanente') ? ' has-error' : '' }}">
                            <label for="permanente" class="col-md-4 control-label">Permanente</label>
                                <input id="permanente" name="permanente"   type="checkbox" {{ ($dadosMensagem->permanente ? 'checked' : '') }}>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Editar
                                </button>
                                <a href="{{URL::previous()}}" class="btn btn-primary">Voltar</a>
                            </div>
                        </div>
                    </form>

                    <div class="row">
                    <ul>
                    
                    </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
