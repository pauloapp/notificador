@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <form class="form-horizontal" method="GET" action="{{ route('mensagem.create') }}">
                    <button type="submit" class="btn btn-success">
                        Nova Mensangem
                    </button>
                </form>
                <div class="panel-heading"><h3><strong>MENSAGENS CADASTRADAS</strong></h3>&nbsp; </div>
                <table class = "table table-striped table-responsive table-bordered table-hover" id = "dataTables-example">
                    <thead>
                    <th>MENSAGEM</th>
                    <th>CÓDIGO</th>
                    <th>CHAVE</th>
                    <th>SITE</th>    
                    <!--th>PERMANENTE</th-->
                    <th></th> 
                     
                    </thead>
                    <tbody>
                        @foreach($mensagensusuario as $item)
                        <tr class = "gradeC">
                            <td title="{{$item->descricao}}">{{str_limit($item->descricao, $limit = 80, $end = '............')}}</td>
                            <td title="Campo utilizado como parâmetro para geração do popup. Identifica o usuário que criou a mensagem.">{{$item->codigo}}</td>
                            <td title="Campo utilizado como parâmetro para geração do popup. Chave única que identifica o site para qual a mensagem cadastrada.">{{$item->chave}}</td>
                            <td><strong>{{$item->site}}</strong></td>
                            <!--td>{{ ($item->permanente ? 'SIM' : 'NÃO') }}</td-->
                            <td><a href="{{ route('mensagem.edit', ['id'=> $item->id]) }}" class="btn btn-danger">Editar</a></td>
                            <td><a href="{{ route('mensagem.destroy', ['id'=> $item->id]) }}" class="btn btn-warning">Excluir</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection