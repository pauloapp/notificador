<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Esse login não está cadastrado.',
    'throttle' => 'Muitas tentativas de login seguindas. Tente novamente em 30 segundos.',

];
